"""
FILE NAME: Util.py
DESCRIPTION
    This is a file where different utility functions and classes will be defined.

VERSION: 1.0
LAST REVISED: 1 - 4 - 2018
"""

import json


class ComplexEncoder(json.JSONEncoder):
    def default(self, obj):
        """
            Auxiliary function to parse the Squad object into JSON format. It overrides the encode function of the
            json package of python, allowing to recursively encode an object.

            :return: a dictionary with the name of the attributes of the object as key and their value as value
            :rtype: dictionary
        """
        if hasattr(obj, 'reprJSON'):
            return obj.reprJSON()
        else:
            return json.JSONEncoder.default(self, obj)
