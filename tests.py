"""
FILE NAME: tests.py                        
DESCRIPTION                              
    This is the unitest program based on pytest for the prototype. It tests the different functions of the 
    squad.py file to make sure that the reading, storing and manipulation of squads works properly.       
                             
VERSION: 1.0
LAST REVISED: 01 - 04 - 2018                
"""

import unittest
import pytest 

from Squad import Squad
from Soldier import Soldier


class ReadJson(unittest.TestCase):

    def test_read_correct_json(self):
        file_name = "Tests/TestExample1.json"
        squad = Squad.read_squad_file(file_name)

        self.assertAlmostEqual(4,
                               squad.number_members,
                               delta=0.01,
                               msg="Incorrect number of members")

        self.assertEqual('SquadExample1',
                         squad.name,
                         msg="Incorrect name of squad!")

        self.assertEqual('Captain1',
                         squad.members[0].name,
                         msg="Incorrect name of captain!")

        self.assertEqual('C005',
                         squad.members[0].items_list[1],
                         msg="Incorrect item of captain!")

        self.assertEqual('H',
                         squad.members[1].type_soldier,
                         msg="Incorrect type of hierophant!")

        self.assertAlmostEqual(100,
                               squad.members[2].cost,
                               delta=0.01,
                               msg="Incorrect cost of Soldier3")

    def test_read_non_existing_json(self):
        file_name = "fdgdfgdfrgdg.json"

        with pytest.raises(SystemExit) as info:
            squad = Squad.read_squad_file(file_name)
            self.assertEqual("Error reading JSON file '" + file_name + "' does not exist.",
                             str(info.value),
                             msg="Error when file does not exist")

    def test_read_empty_json(self):
        file_name = "TestExample2.json"

        with pytest.raises(SystemExit) as info:
            squad = Squad.read_squad_file(file_name)
            self.assertEqual("File '" + file_name + "' is empty",
                             str(info.value),
                             msg="Error when file is empty")

    def test_read_non_valid_json(self):
        file_name = "TestExample3.json"

        with pytest.raises(SystemExit) as info:
            squad = Squad.read_squad_file(file_name)
            self.assertEqual("File '" + file_name + "' does not have the right format",
                             str(info.value),
                             msg="Error when file is not valid")

        file_name = "TestExample4.json"

        with pytest.raises(SystemExit) as info:
            squad = Squad.read_squad_file(file_name)
            self.assertEqual("File '" + file_name + "' does not have the right format",
                             str(info.value),
                             msg="Error when file is not valid")


class ModifySquad(unittest.TestCase):

    def test_add_members_squad(self):
        file_name = "Tests/TestExample1.json"
        squad = Squad.read_squad_file(file_name)

        squad.add_member(Soldier('SoldierTest', 1, 2, 3, 4, 5, 6, 7, None, 'S'))

        self.assertAlmostEqual(5,
                               squad.number_members,
                               delta=0.01,
                               msg="Incorrect number of members")

        self.assertAlmostEqual(5,
                               squad.members[4].morale,
                               delta=0.01,
                               msg="Incorrect morale value")

        added = squad.add_member(Soldier('SoldierTest', 1, 2, 3, 4, 5, 6, 7, None, 'S'))

        self.assertEqual(added,
                         False,
                         msg="Incorrect addition of soldier")

    def test_remove_members_squad(self):
        file_name = "Tests/TestExample1.json"
        squad = Squad.read_squad_file(file_name)

        squad.remove_member('Captain1')
        squad.remove_member('Hierophant2')
        squad.remove_member('Soldier3')

        self.assertAlmostEqual(1,
                               squad.number_members,
                               delta=0.01,
                               msg="Incorrect number of members")

        squad.remove_member('Soldier4')

        self.assertAlmostEqual(0,
                               squad.number_members,
                               delta=0.01,
                               msg="Incorrect number of members")

        removed = squad.remove_member('Soldier4')

        self.assertEqual(removed,
                         False,
                         msg="Incorrect removal of non existing soldier")

    def test_contains_members_squad(self):
        file_name = "Tests/TestExample1.json"
        squad = Squad.read_squad_file(file_name)

        exists = squad.contains_member('Hierophant2')
        self.assertEqual(exists,
                         True,
                         msg="Incorrect contains of Hierophant2")

        exists = squad.contains_member('Captain1')
        self.assertEqual(exists,
                         True,
                         msg="Incorrect contains of Captain1")

        exists = squad.contains_member('Captain85')
        self.assertEqual(exists,
                         False,
                         msg="Incorrect contains of Captain85")


class StoreJson(unittest.TestCase):

    def test_store_json(self):
        file_name = "Tests/TestExample1.json"
        squad = Squad.read_squad_file(file_name)
        squad.remove_member('Captain1')
        squad.remove_member('Soldier3')
        squad.add_member(Soldier('SoldierTest', 1, 2, 3, 4, 5, 6, 7, None, 'S'))

        file_name_store = 'UnitTestStoreExample.json'
        squad.store_squad_file(file_name_store, squad)
        squad_new = Squad.read_squad_file(file_name_store)

        self.assertAlmostEqual(3,
                               squad_new.number_members,
                               delta=0.01,
                               msg="Incorrect number of members")

        self.assertEqual('SoldierTest',
                         squad_new.members[2].name,
                         msg="Incorrect name of captain!")
