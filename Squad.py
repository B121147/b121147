"""
FILE NAME: Squad.py
DESCRIPTION
    This is main class for the squad. It reads data from a JSON file, stores it and allows to change it
    by adding or deleting soldiers. It also allows to access the members of the squad directly and modify them.

VERSION: 1.0
LAST REVISED: 1 - 4 - 2018
"""

import json
import jsonschema
from pprint import pprint
from Soldier import Soldier
from Captain import Captain
from Hierophant import Hierophant
from Util import ComplexEncoder


class Squad:

    def __init__(self, name, number_members):
        self.name = name
        self.number_members = number_members
        self.members = None

    def add_member(self, member):
        """
            This function adds new members to the squad. There can be only one captain or hierophant
            so it checks if there is already one in the squad.

            :parameter member: new member to be inserted
            :return: true if it was successfully inserted, false if not
            :rtype: boolean
        """

        # If there is already another soldier with the same name, it cannot be inserted
        if self.contains_member(member.name):
            print("That member is already in the squad.")
            return False

        # If it is a soldier it can be inserted
        if member.type_soldier == 'S':
            self.members.append(member)
            self.number_members += 1
            return True

        # If it is a captain of hierophant check if there is already one
        elif member.type_soldier == 'C' or member.type_soldier == 'H':
            if any(current_member.type_soldier == 'C' for current_member in self.members):
                print("There is already one captain in this squad.")
                return False
            if any(current_member.type_soldier == 'H' for current_member in self.members):
                print("There is already one hierophant in this squad.")
                return False
            else:
                self.members.append(member)
                self.number_members += 1
                return True

    def remove_member(self, name):
        """
            This function removes members from the squad given a soldier name. It does not exist the function
            does nothing.

            :parameter name: name of the member to be removed
            :return: true if it was successfully removed, false if it does not exist
            :rtype: boolean
        """
        if self.members is not None:
            for member in self.members:
                if member.name == name:
                    self.members.remove(member)
                    self.number_members -= 1
                    return True
        return False

    def contains_member(self, name):
        """
            This function returns true if the soldier whose name is passed as an argument exists and false if not

            :parameter name: name of the member to checked
            :return: true if it exists, false if not
            :rtype: boolean
        """
        if any(current_member.name == name for current_member in self.members):
            return True
        else:
            return False

    def reprJSON(self):
        """
            Auxiliary function to parse the Squad object into JSON format

            :return: a dictionary with the name of the attributes of the object as key and their value as value
            :rtype: dictionary
        """
        return dict(name=self.name,
                    number_members=self.number_members,
                    members=self.members)


    def print_squad_data(self):
        """
            This function prints the name, numbers of members and attributes of the soldiers of the squad.

        """
        print(self.name)
        print(self.number_members)
        if self.members is not None:
            for member in self.members:
                pprint(vars(member))

    @staticmethod
    def read_squad_file(file_name):
        """
            This function reads a JSON file and validates it against a schema to ensure that the data is valid.
            It returns a Squad object filled with the data of the file.

            :parameter file_name: JSON file containing the data of the squad
            :return: a Squad object filled with the data of the file
            :rtype: Squad object
        """

        # Name of the schema provided for validating the squad files, change if required
        schema_name = 'schema_squad.json'

        # Read JSON schema file
        try:
            with open(schema_name, 'r') as schema_file:
                schema_data_string = schema_file.read()
            schema = json.loads(schema_data_string)
        except IOError:
            print("Error reading JSON schema file. '" + schema_name + "' does not exist.")
            exit(-1)

        # Read JSON squad file and validate it
        try:
            with open(file_name, 'r') as squad_file:
                squad_data_string = squad_file.read()
            squad_data = json.loads(squad_data_string)
            jsonschema.validate(squad_data, schema)
        except IOError:
            print("Error reading JSON file '" + file_name + "' does not exist.")
            exit(-1)
        except json.decoder.JSONDecodeError:
            if not squad_data_string:
                print("File '" + file_name + "' is empty")
            else:
                print("File '" + file_name + "' does not have the right format")
            exit(-1)
        except jsonschema.exceptions.ValidationError:
            print("'" + schema_name + "' does not validate '" + file_name + "'")
            exit(-1)

        # Fill a Squad object with the data of the JSON
        squad = Squad(squad_data['name'], squad_data['number_members'])
        if squad_data['number_members'] == 0:
            squad.members = ()
        else:
            members = list()
            for member in squad_data['members']:
                if member['type_soldier'] == 'S':
                    new_soldier = Soldier(member['name'],
                                          member['move'],
                                          member['fight'],
                                          member['shoot'],
                                          member['armour'],
                                          member['morale'],
                                          member['health'],
                                          member['cost'],
                                          member['notes']
                                          if 'notes' in member else None,
                                          member['type_soldier']
                                          )
                    members.append(new_soldier)
                elif member['type_soldier'] == 'C':
                    new_soldier = Captain(member['name'],
                                          member['move'],
                                          member['fight'],
                                          member['shoot'],
                                          member['armour'],
                                          member['morale'],
                                          member['health'],
                                          member['cost'],
                                          member['notes'] if 'notes' in member else None,
                                          member['type_soldier'],
                                          member['experience'] if 'experience' in member else None,
                                          member['items_slots'] if 'items_slots' in member else None,
                                          member['items_list'] if 'items_list' in member else None,
                                          member['skills_number'] if 'skills_number' in member else None,
                                          member['skills_list'] if 'skills_list' in member else None
                                          )
                    members.append(new_soldier)
                elif member['type_soldier'] == 'H':
                    print(member['items_slots'])
                    print(member['items_list'][0])
                    new_soldier = Hierophant(member['name'],
                                             member['move'],
                                             member['fight'],
                                             member['shoot'],
                                             member['armour'],
                                             member['morale'],
                                             member['health'],
                                             member['cost'],
                                             member['notes'] if 'notes' in member else None,
                                             member['type_soldier'],
                                             member['experience'] if 'experience' in member else None,
                                             member['items_slots'] if 'items_slots' in member else None,
                                             member['items_list'] if 'items_list' in member else None,
                                             member['skills_number'] if 'skills_number' in member else None,
                                             member['skills_list'] if 'skills_list' in member else None
                                             )
                    members.append(new_soldier)

            squad.members = members

        return squad

    @staticmethod
    def store_squad_file(file_name, squad):
        """
            This function stores the data of a Squad object in a JSON file following the Squad schema.

            :parameter file_name: JSON file containing the data of the squad
            :parameter squad: object Squad containing the data of the squad
            :return: an Squad object filled with the data of the file
            :rtype: Squad object
        """

        # Name of the schema provided for validating the squad files, change if required
        schema_name = 'schema_squad.json'

        # Read JSON schema file
        try:
            with open(schema_name, 'r') as schema_file:
                schema_data_string = schema_file.read()
            schema = json.loads(schema_data_string)
        except IOError:
            print("Error reading JSON schema file. '" + schema_name + "' does not exist.")
            exit(-1)

        # Transform Squad object in JSON file and validate it against the schema
        try:
            print()
            squad_data_string = json.dumps(squad.reprJSON(), cls=ComplexEncoder)
            print(squad_data_string)
            squad_data = json.loads(squad_data_string)
            jsonschema.validate(squad_data, schema)
            with open(file_name, 'w') as squad_file:
                json.dump(squad_data, squad_file, indent=1)
        except IOError:
            print("Error reading JSON file. " + file_name + " does not exist.")
            exit(-1)
        except jsonschema.exceptions.ValidationError:
            print("'" + schema_name + "' does not validate '" + file_name + "'")
            exit(-1)
