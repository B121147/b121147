"""
FILE NAME: Soldier.py
DESCRIPTION
    This is main class for the soldiers. It stores data for the soldiers allowing to create objects containing
    their attributes such as name, fight and shoot.

VERSION: 1.0
LAST REVISED: 1 - 4 - 2018
"""


class Soldier:

    def __init__(self,
                 name,
                 move,
                 fight,
                 shoot,
                 armour,
                 morale,
                 health,
                 cost,
                 notes,
                 type_soldier):
        self.name = name
        self.move = move
        self.fight = fight
        self.shoot = shoot
        self.armour = armour
        self.morale = morale
        self.health = health
        self.cost = cost
        self.notes = notes
        self.type_soldier = type_soldier

    def reprJSON(self):
        """
            Auxiliary function to parse the Squad object into JSON format

            :return: a dictionary with the name of the attributes of the object as key and their value as value
            :rtype: dictionary
        """
        return dict(name=self.name,
                    move=self.move,
                    fight=self.fight,
                    shoot=self.shoot,
                    armour=self.armour,
                    morale=self.morale,
                    health=self.health,
                    cost=self.cost,
                    type_soldier=self.type_soldier
                    )
