"""
FILE NAME: Captain.py
DESCRIPTION
    This is main class for the hierophant. It inherits from the Soldier class, as it shares many attributes,
    but includes special ones: experience, skills and items.

VERSION: 1.0
LAST REVISED: 1 - 4 - 2018
"""

from Soldier import Soldier


class Hierophant(Soldier):

    def __init__(self,
                 name,
                 move,
                 fight,
                 shoot,
                 armour,
                 morale,
                 health,
                 cost,
                 notes,
                 type_soldier,
                 experience,
                 items_slots,
                 items_list,
                 skills_number,
                 skills_list):
        Soldier.__init__(self,
                         name,
                         move,
                         fight,
                         shoot,
                         armour,
                         morale,
                         health,
                         cost,
                         notes,
                         type_soldier)
        self.experience = experience
        self.items_slots = items_slots
        self.items_list = items_list
        self.skills_number = skills_number
        self.skills_list = skills_list

    def reprJSON(self):
        """
            Auxiliary function to parse the Squad object into JSON format

            :return: a dictionary with the name of the attributes of the object as key and their value as value
            :rtype: dictionary
        """
        return dict(name=self.name,
                    move=self.move,
                    fight=self.fight,
                    shoot=self.shoot,
                    armour=self.armour,
                    morale=self.morale,
                    health=self.health,
                    cost=self.cost,
                    type_soldier=self.type_soldier,
                    experience=self.experience,
                    items_slots=self.items_slots,
                    items_list=self.items_list,
                    skills_number=self.skills_number,
                    skills_list=self.skills_list,
                    )

